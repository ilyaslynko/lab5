using System;
using System.Collections.Generic;
using System.Text;
using lab5.Models;

using ReactiveUI;
using System.Windows.Input;
using System.Threading.Tasks;
using RestSharp;
using Newtonsoft.Json;
using Avalonia.Media.Imaging;
using Avalonia.Data.Converters;
using System.Collections.ObjectModel;
using System.Reactive;
using Avalonia;
using System.Net;
using Avalonia.Platform;
using System.IO;

namespace lab5.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private Repositories _selectedReps;
        public Repositories SelectedReps
        {
            get => _selectedReps;
            set
            {
                _selectedReps = value;
                OnPropertyChanged(nameof(SelectedReps));
            }
        }

        private string _searchText;

        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                OnPropertyChanged(nameof(SearchText));
            }
        }

        private bool _StatusBar;
        public bool StatusBar
        {
            get => _StatusBar;
            set
            {
                _StatusBar = value;
                OnPropertyChanged(nameof(StatusBar));
            }
        }

        private string _Login;
        public string Login
        {
            get => _Login;
            set
            {
                _Login = value;
                OnPropertyChanged(nameof(Login));
            }
        }

        private string _Name;
        public string Name
        {
            get => _Name;
            set
            {
                _Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        private string _CountRepos;
        public string CountRepos
        {
            get => _CountRepos;
            set
            {
                _CountRepos = value;
                OnPropertyChanged(nameof(CountRepos));
            }
        }



        private string _Followers;
        public string Followers
        {
            get => _Followers;
            set
            {
                _Followers = value;
                OnPropertyChanged(nameof(Followers));
            }
        }

        private Bitmap _Avatar;
        public Bitmap Avatar
        {
            get => _Avatar;
            set
            {
                _Avatar = value;
                OnPropertyChanged(nameof(Avatar));
            }
        }

        private User _Data { get; set; } = new User();
        private ObservableCollection<Repositories> Repository { get; set; } = new ObservableCollection<Repositories>();
        private ObservableCollection<Commits> CommitSet { get; set; } = new ObservableCollection<Commits>();

        public MainWindowViewModel()
        {
            Command = ReactiveCommand.Create(async () =>
            {
                await Task.Run(() => {
                    StatusBar = true;
                    var query = new RestClient("https://api.github.com/users/" + SearchText);
                    var request = new RestRequest(Method.GET);
                    var response = query.Execute(request);

                    _Data = JsonConvert.DeserializeObject<User>(response.Content);
                    if (_Data.login != null)
                    {
                        Login = _Data.login;
                        CountRepos = _Data.public_repos;
                        Followers = _Data.followers;
                        Name = _Data.name;
                        var assets = AvaloniaLocator.Current.GetService<IAssetLoader>();
                        query = new RestClient("https://api.github.com/users/" + _Data.login + "/repos");
                        request = new RestRequest(Method.GET);
                        byte[] avatarBytes = new WebClient().DownloadData(_Data.avatar_url);
                        using (MemoryStream memory = new MemoryStream(avatarBytes, 0, avatarBytes.Length))
                        {
                            Avatar = new Avalonia.Media.Imaging.Bitmap(memory);
                        }
                        var Repositories = query.Execute<List<Repositories>>(request);
                        foreach (Repositories i in Repositories.Data)
                        {
                            Repository.Add(i);
                        }
                    }
                    else
                    {
                        Login = "";
                        CountRepos = "";
                        Followers = "";
                        Name = "";
                        Avatar = null;
                        Repository.Clear();
                        CommitSet.Clear();
                    }
                    StatusBar = false;
                });


            });
            DoTheThing = ReactiveCommand.Create<Repositories>(RunTheThing);
        }



        public async void RunTheThing(Repositories parameter)
        {
            await Task.Run(() =>
            {
                StatusBar = true;
                CommitSet.Clear();

                var client = new RestClient("https://api.github.com/repos/" + Login + "/" + parameter.name + "/commits");
                var request = new RestRequest(Method.GET);
                var Repositories = client.Execute<List<Commits>>(request);

                foreach (Commits i in Repositories.Data)
                {
                    i.commit.author.formatDate();
                    CommitSet.Add(i);
                }

                StatusBar = false;
            });
        }
        public ReactiveCommand<Repositories, Unit> DoTheThing { get; }
        public ICommand Command { get; }
    }
}
