﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5.Models
{
    public class User
    {
        public string login { get; set; }
        public string name { get; set; }
        public string avatar_url { get; set; }
        public string repos_url { get; set; }
        public string public_repos { get; set; }
        public string followers { get; set; }
    }
}
