﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5.Models
{
    public class Author
    {
        public string name { get; set; }
        public DateTime date { get; set; }

        public void formatDate()
        {
            date.ToLongDateString();
        }
    }
}
