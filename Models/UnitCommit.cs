﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5.Models
{
     public class UnitCommit
    {
        public Author author { get; set; }
        public string message { get; set; }
    }
}
